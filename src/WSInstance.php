<?php

namespace Lar\WS;

use Lar\WS\WebSockets\OnEvent;

/**
 * Class WS
 * @package Lar\WS
 */
class WSInstance {

    /**
     * @param  \Closure  $closure
     * @return $this
     */
    public function onMessage(\Closure $closure)
    {
        OnEvent::message($closure);

        return $this;
    }

    /**
     * @param  \Closure  $closure
     * @return $this
     */
    public function onClose(\Closure $closure)
    {
        OnEvent::close($closure);

        return $this;
    }

    /**
     * @param  \Closure  $closure
     * @return $this
     */
    public function onSend(\Closure $closure)
    {
        OnEvent::send($closure);

        return $this;
    }
}