<?php

namespace Lar\WS\WebSockets;

use BeyondCode\LaravelWebSockets\WebSockets\WebSocketHandler as WebSocketOriginHandler;
use Ratchet\ConnectionInterface;
use Ratchet\RFC6455\Messaging\MessageInterface;

/**
 * Class WebSocketHandler
 * @package Lar\WS\WebSockets
 */
class WebSocketHandler extends WebSocketOriginHandler
{
    /**
     * @param  ConnectionInterface  $connection
     * @param  MessageInterface  $message
     */
    public function onMessage(ConnectionInterface $connection, MessageInterface $message)
    {
        parent::onMessage($connection, $message);
        OnEvent::callMessageEvents(
            json_decode($message->getPayload(), true),
            $connection
        );
    }
}