<?php

namespace Lar\WS\WebSockets;

use Ratchet\ConnectionInterface;

class ConnectionLoggerOverride extends \BeyondCode\LaravelWebSockets\Server\Logger\ConnectionLogger
{
    /**
     * @return string|null
     */
    public function socketId()
    {
        return $this->connection->socketId ?? null;
    }

    public function send($data)
    {
        $socketId = $this->connection->socketId ?? null;

        $this->info("Connection id {$socketId} sending message {$data}");

        OnEvent::callSendEvents($data, $this);

        $this->connection->send($data);
    }

    public function close()
    {
        $this->warn("Connection id {$this->connection->socketId} closing.");

        OnEvent::callCloseEvents($this);

        $this->connection->close();
    }
}
