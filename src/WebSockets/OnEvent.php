<?php

namespace Lar\WS\WebSockets;

use Ratchet\ConnectionInterface;

/**
 * Class OnMessage
 * @package Lar\WS\WebSockets
 */
class OnEvent
{
    /**
     * @var \Closure[]
     */
    protected static $on_message_events = [];

    /**
     * @var \Closure[]
     */
    protected static $on_close_events = [];

    /**
     * @var \Closure[]
     */
    protected static $on_send_events = [];

    /**
     * @param  \Closure  $closure
     */
    public static function message(\Closure $closure)
    {
        static::$on_message_events[] = $closure;
    }

    /**
     * @param  \Closure  $closure
     */
    public static function close(\Closure $closure)
    {
        static::$on_close_events[] = $closure;
    }

    /**
     * @param  \Closure  $closure
     */
    public static function send(\Closure $closure)
    {
        static::$on_send_events[] = $closure;
    }

    /**
     * @param  array|null  $message
     * @param  ConnectionInterface  $connection
     */
    public static function callMessageEvents(array $message, ConnectionInterface $connection)
    {
        foreach (static::$on_message_events as $on_message_event) {
            $on_message_event($message, $connection);
        }
    }

    /**
     * @param  ConnectionInterface  $connection
     */
    public static function callCloseEvents(ConnectionInterface $connection)
    {
        foreach (static::$on_close_events as $on_message_event) {
            $on_message_event($connection);
        }
    }

    /**
     * @param $data
     * @param  ConnectionInterface  $connection
     */
    public static function callSendEvents($data, ConnectionInterface $connection)
    {
        foreach (static::$on_send_events as $on_send_event) {
            $on_send_event($data, $connection);
        }
    }
}