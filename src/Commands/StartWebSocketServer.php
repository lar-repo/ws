<?php

namespace Lar\WS\Commands;

use BeyondCode\LaravelWebSockets\Console\StartWebSocketServer as StartWebSocketServerOriginal;
use BeyondCode\LaravelWebSockets\Server\Logger\ConnectionLogger;
use Lar\WS\WebSockets\ConnectionLoggerOverride;

class StartWebSocketServer extends StartWebSocketServerOriginal {

    protected function configureConnectionLogger()
    {
        app()->bind(ConnectionLogger::class, function () {
            return (new ConnectionLoggerOverride($this->output))
                ->enable(config('app.debug'))
                ->verbose($this->output->isVerbose());
        });

        return $this;
    }
}