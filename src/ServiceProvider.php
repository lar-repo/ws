<?php

namespace Lar\WS;

use BeyondCode\LaravelWebSockets\Console\StartWebSocketServer;
use BeyondCode\LaravelWebSockets\Server\Logger\ConnectionLogger;
use Illuminate\Support\ServiceProvider as ServiceProviderIlluminate;
use Illuminate\Support\Facades\Broadcast;
use Lar\Layout\Core\LConfigs;
use Lar\WS\WebSockets\Route;

/**
 * Class ServiceProvider
 * @package Lar\WS
 */
class ServiceProvider extends ServiceProviderIlluminate
{
    /**
     * @var array
     */
    protected $commands = [

    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [

    ];

    /**
     * Bootstrap services.
     *
     * @return void
     * @throws \Exception
     */
    public function boot()
    {
        /**
         * Register publishers configs
         */
        $this->publishes([
            __DIR__.'/../config/lar-ws.php' => config_path()
        ], 'lar-ws');

        $this->app->singleton('websockets.router', function () {
            return new Route();
        });

        Broadcast::routes();

        require base_path('routes/channels.php');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(StartWebSocketServer::class, \Lar\WS\Commands\StartWebSocketServer::class);

        $this->mergeConfigFrom(
            __DIR__.'/../config/lar-ws.php', 'lar-ws'
        );
        $this->registerRouteMiddleware();
        $this->commands($this->commands);
        $this->create_configs();
    }

    /**
     * Create broadcasting configs
     */
    protected function create_configs()
    {
        $def = config('lar-ws.pusher', []);
        $url = explode("://", config('app.url'));
        $def['options']['scheme'] = !isset($def['options']['scheme']) ? ($url[0] ?? 'http') : $def['options']['scheme'];
        $def['options']['host'] = '127.0.0.1';
        $def['options']['port'] = !isset($def['options']['port']) ? config('websockets.dashboard.port', 6001) : $def['options']['port'];
        $def['options']['encrypted'] = !isset($def['options']['encrypted']) ? true : $def['options']['encrypted'];
        config(\Arr::dot($def, 'broadcasting.connections.pusher.'));
        config(['broadcasting.default' => config('lar-ws.default', 'pusher')]);
        config(['websockets.max_request_size_in_kb' => config('lar-ws.max_request_size_in_kb', 250)]);
        config(['websockets.app_provider' => config('lar-ws.app_provider', \BeyondCode\LaravelWebSockets\Apps\ConfigAppProvider::class)]);
        config(['websockets.channel_manager' => config('lar-ws.channel_manager', \BeyondCode\LaravelWebSockets\WebSockets\Channels\ChannelManagers\ArrayChannelManager::class)]);
        config(\Arr::dot(config('lar-ws.apps', []), 'websockets.apps.'));
        config(\Arr::dot(config('lar-ws.ssl', []), 'websockets.ssl.'));
        config(\Arr::dot(config('lar-ws.allowed_origins', []), 'websockets.allowed_origins.'));
        if ($def['options']['port'] !== 6001) {
            LConfigs::add('ws_port', $def['options']['port']);
        }
        LConfigs::add('ws_app_key', config('broadcasting.connections.pusher.key'));
        LConfigs::add('ws_app_cluster', config('broadcasting.connections.pusher.options.cluster'));
    }

    /**
     * Register the route middleware.
     *
     * @return void
     */
    protected function registerRouteMiddleware()
    {
        // register route middleware.
        foreach ($this->routeMiddleware as $key => $middleware) {

            app('router')->aliasMiddleware($key, $middleware);
        }
    }
}

